import React, { Component } from 'react'
import Header from './component/layout/header'
import 'bootstrap/dist/css/bootstrap.min.css';

import TransferList from './component/contacts/Contact';


class App extends Component {
    render() {
        return ( 
            
            <div className = "App" style={{background:"#505050",
                        position: 'fixed',
                        top: '0',
                        left: '0',
                        bottom: '0',
                        right: '0',
                        overflow: 'auto'
                    }}>
                <Header branding = "Contact Manager" />
                    <div >
                        <TransferList />
                    </div>
             </div> 
           

        );
    }
}
export default App;